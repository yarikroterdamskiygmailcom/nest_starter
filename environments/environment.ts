export const environment = {
  Strategy: {
    secretKey: "secretKey",
    defaultStrategy: 'jwt',
    expiresIn: 3600
  },
};
