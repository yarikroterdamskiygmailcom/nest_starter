import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { CatchAll } from './shared/decorators/catch_decorator_method';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('test')
  @CatchAll((err, ctx) => console.log(ctx, err))
  testMethod(): string {
    return 'hello';
  }
}
