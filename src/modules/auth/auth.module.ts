import {Module} from '@nestjs/common';
import {AuthService} from './auth.service';
import {TypeOrmModule} from '@nestjs/typeorm';
import {Users} from './entity/users.entity';
import {PassportModule} from '@nestjs/passport';
import {JwtModule} from '@nestjs/jwt';
import {AuthController} from './auth.controller';
import {JwtStrategy} from "../../strategies/jwt.startegy";
import {environment} from "../../../environments/environment";


@Module({
    imports: [
        PassportModule.register({
            defaultStrategy: environment.Strategy.defaultStrategy
        }),
        JwtModule.register({
            secret: environment.Strategy.secretKey,
            signOptions: {
                expiresIn: environment.Strategy.expiresIn,
            },
        }),
        TypeOrmModule.forFeature([Users]),
    ],
    controllers: [AuthController],
    providers: [AuthService, JwtStrategy],
    exports: [AuthService],
})
export class AuthModule {
}
